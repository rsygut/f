﻿open System

let rec readlines () = seq {
  let line = Console.ReadLine()
  if line <> null then
      yield line
      yield! readlines ()
}

for line in readlines() do
let y = line.Split ' ' |> Seq.map Double.Parse |> Seq.toArray
let delta = y.[1] *y.[1] - 4.0 * y.[0] * y.[2]
let posNeg = if delta > 0.0 then 2 elif delta < 0.0 then 0 else 1 
printfn "%i" posNeg

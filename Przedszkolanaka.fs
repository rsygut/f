﻿open System

let rec doTheStuff currTest maxTest = 
    if currTest < maxTest then
        let line = Console.ReadLine()
        let y = line.Split ' '
             |> Seq.map System.Int32.Parse
             |> Seq.sort
             |> Seq.toArray
        let  mutable  wynik = y.[1]
        while(wynik % y.[0] <> 0 || wynik % y.[1] <> 0) do
            wynik <- wynik + y.[1]
        printfn "%A"  wynik
        doTheStuff (currTest+1) maxTest
        

[<EntryPoint>]
let main argv = 
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    doTheStuff 0 numCases
  
    0
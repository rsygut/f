﻿open System

let rec doTheStuff currTest maxTest = 
    if currTest < maxTest then
        let line = Console.ReadLine()
        let y = line.Split ' ' |> Seq.map Int32.Parse |> Seq.toArray
        let n = y.[0]
        let c = n - 1
        if c > y.[1] then printfn "TAK" elif c = 0 && y.[1]> 0 then printfn "TAK" elif y.[1] = 0 then printfn "NIE" elif y.[1] % c > 0 then printfn "TAK"else printfn "NIE"
        
        doTheStuff (currTest+1) maxTest


  
[<EntryPoint>]
let main argv = 
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    doTheStuff 0 numCases
 
    0


    
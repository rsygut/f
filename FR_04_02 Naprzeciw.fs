﻿open System

let rec doTheStuff currTest maxTest = 
    if currTest < maxTest then
        let line = Console.ReadLine()
        let y = line.Split ' ' |> Seq.map Int32.Parse |> Seq.toList
        let n = y.[0]
        let k = y.[1]
        if (n % 2) = 1 then Console.WriteLine("BRAK") elif (k > n / 2) then Console.WriteLine(k - n/2) else Console.WriteLine(k + n/2)
        doTheStuff (currTest+1) maxTest

[<EntryPoint>]
let main argv = 
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    doTheStuff 0 numCases
 
    0
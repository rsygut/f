﻿open System

let rec readlines () = seq {
  let line = Console.ReadLine()
  if line <> null then
      yield line
      yield! readlines ()
}

for line in readlines() do
let tab = line.Split ' ' |> Seq.map Int32.Parse |> Seq.sort |> Seq.toArray
if tab.[0] + tab.[1] <= tab.[2] then printfn "brak" elif tab.[0] * tab.[0] + tab.[1] * tab.[1] = tab.[2] * tab.[2]  then printfn "prostokatny" elif tab.[0] * tab.[0] + tab.[1] * tab.[1] > tab.[2] * tab.[2]  then printfn "ostrokatny" else printfn "rozwartokatny"

﻿open System

let rec doTheStuff currTest maxTest = 
    if currTest < maxTest then
        let line = Console.ReadLine()
        let y = Int32.Parse (line)
        printfn "%i" (y * y)
        doTheStuff (currTest+1) maxTest

[<EntryPoint>]
let main argv = 
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    doTheStuff 0 numCases
    0